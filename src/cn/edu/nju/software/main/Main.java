package cn.edu.nju.software.main;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import cn.edu.nju.software.constant.Settings;
import cn.edu.nju.software.ds.ImageInfo;
import cn.edu.nju.software.ds.Instance;
import cn.edu.nju.software.inputformat.MyFileInputFormat;
import cn.edu.nju.software.util.StringUtil;
import cn.edu.nju.software.util.Similarity;

public class Main {

	public static class MatcherMapper extends
			Mapper<Text, ImageInfo, IntWritable, IntWritable> {

		Vector<Instance> centers = new Vector<Instance>();
		List<StringUtil> indexFile = new ArrayList<StringUtil>();

		public void setup(Context context) {
			this.initCenters(context);
			this.initReverseSort(context);
		}

		private void initReverseSort(Context context) {
			String file = Settings.REVERSE_SORT__FILE;
			BufferedReader in = null;
			try {
				FileSystem fs;

				fs = FileSystem.get(URI.create(file),
						context.getConfiguration());
				InputStream is = fs.open(new Path(file));// ��ȡ�ļ�
				in = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = in.readLine()) != null) {
					line = line.trim();
					String[] splitStr = line.split("\t");

					if (splitStr.length == 2) {
						StringUtil indexLine = new StringUtil(splitStr[0],
								splitStr[1]);
						indexFile.add(indexLine);
					}
				}
				in.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		private void initCenters(Context context) {
			String file = Settings.CENTER__FILE;
			BufferedReader in = null;
			try {
				FileSystem fs;

				fs = FileSystem.get(URI.create(file),
						context.getConfiguration());
				InputStream is = fs.open(new Path(file));
				in = new BufferedReader(new InputStreamReader(is));
				String line;
				while ((line = in.readLine()) != null) {
					Instance sample = new Instance(line);
					centers.add(sample);
				}
				in.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		int count = 0;

		public void map(Text key, ImageInfo value, Context context)
				throws IOException, InterruptedException {
			Map<String, Integer> map = new HashMap<String, Integer>();
			String[] vector = value.vectors;
			for (String line : vector) {
				if (line.isEmpty()) {
					continue;
				}
				Instance sample = new Instance(line);
				int index = -1;
				double minDist = Double.MAX_VALUE;
				for (int i = 0; i < centers.size(); i++) {
					double dist = Instance.getEulerDist(sample, centers.get(i));
					if (dist < minDist) {
						minDist = dist;
						index = i;
					}
				}
				String mapkey = "" + (index + 1);
				if (map.containsKey(mapkey)) {
					map.put(mapkey, map.get(mapkey) + 1);
				} else {
					map.put(mapkey, 1);
				}
			}

			int position = Similarity.getImgPosition(this.indexFile,
					key.toString(), map);		
			// output position to context & compute average,middle in reduce
			context.write(new IntWritable(1), new IntWritable(position));

		}
	}

	public static class MatcherReducer extends
			Reducer<IntWritable, IntWritable, IntWritable, Text> {

		public void reduce(IntWritable key, Iterable<IntWritable> values,
				Context context) throws IOException, InterruptedException {
			ArrayList tempList = new ArrayList();
			if (values != null) {
				Iterator iterator = values.iterator();
				while (iterator.hasNext()) {
					int i = ((IntWritable) iterator.next()).get();
					tempList.add(i);
				}
			}
			System.out.println(computeAverage(tempList));
			System.out.println(computeMiddle(tempList));

		}

		public double computeAverage(ArrayList array) {
			int result = 0;
			for (int i = 0; i < array.size(); i++) {
				result += (int) array.get(i);
			}
			return result / array.size();
		}

		public double computeMiddle(ArrayList array) {
			Collections.sort(array);
			int length = array.size();
			double result = (double) (length % 2 == 0 ? (((int) array
					.get(length / 2 - 1) + (int) array.get(length / 2)) / 2) * 1.0
					: (int) array.get(length / 2) * 1.0);
			return result;
		}
	}

	public static void main(String[] args) {
		Configuration conf = new Configuration();
		Job job;
		try {
			job = new Job(conf);
			job.setJarByClass(Main.class);
			FileInputFormat.addInputPath(job, new Path(Settings.DATA_FILE));
			FileOutputFormat.setOutputPath(job,
					new Path(Settings.OUTPUT_FOLDER));
			job.setMapperClass(MatcherMapper.class);
			job.setOutputKeyClass(IntWritable.class);
			job.setOutputValueClass(IntWritable.class);
			job.setInputFormatClass(MyFileInputFormat.class);
			job.setReducerClass(MatcherReducer.class);
			System.exit(job.waitForCompletion(true) ? 0 : 1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}