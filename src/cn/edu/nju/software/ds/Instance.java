package cn.edu.nju.software.ds;

public class Instance{
	
    public static final int DIMENTION=24;
    public double arr[];
    
    public Instance(){
    	arr=new double[DIMENTION];
    }
    
    public Instance(String line)
    {
    	arr=new double[DIMENTION];
    	String[] str = line.split(" ");
		for (int i = 0; i < str.length; i++) {
			arr[i] = Double.parseDouble(str[i]);
		}
    }
    
    public void setArr(double arr[])
    {
    	this.arr = arr;
    }
    
	public static double getEulerDist(Instance vec1, Instance vec2) {
		if (!(vec1.arr.length == DIMENTION && vec2.arr.length == DIMENTION)) {
			System.exit(1);
		}
		double dist = 0.0;
		for (int i = 0; i < DIMENTION; ++i) {
			dist += (vec1.arr[i] - vec2.arr[i]) * (vec1.arr[i] - vec2.arr[i]);
		}
		return Math.sqrt(dist);
	}
	
	public void clear() {
		for (int i = 0; i < arr.length; i++)
			arr[i] = 0.0;
	}
	
	@Override
	public String toString() {
		String rect = String.valueOf(arr[0]);
		for (int i = 1; i < DIMENTION; i++)
			rect += " " + String.valueOf(arr[i]);
		return rect;
	}
}