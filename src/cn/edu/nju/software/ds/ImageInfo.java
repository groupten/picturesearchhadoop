package cn.edu.nju.software.ds;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;


public class ImageInfo implements Writable{
	public String[] vectors;
	
	public ImageInfo(String[] vectors) {
		this.vectors = vectors;
	}
	
	public ImageInfo() {
		vectors = new String[4];
	}
	
	@Override
	public void readFields(DataInput in) throws IOException {
		for(int i=0;i<4;i++){
			vectors[i] = in.readLine();
		}
	}
	@Override
	public void write(DataOutput out) throws IOException {
		for(int i=0;i<vectors.length;i++){
			out.writeBytes(vectors[i]);
		}
	}

	public String[] getVectors() {
		return vectors;
	}

	public void setVectors(String[] vectors) {
		this.vectors = vectors;
	}
	
	public String toString(){
		String result = "";
		for(int i=0;i<vectors.length;i++){
			result = result + vectors[i] + ",";
		}
		return result;
	}


}