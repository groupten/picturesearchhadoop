package cn.edu.nju.software.constant;

public class Settings {

	public static String CENTER__FILE = "hdfs://localhost:9000/user/group10/input/centers.txt";
	public static String REVERSE_SORT__FILE = "hdfs://localhost:9000/user/group10/input/reverse_sort.txt";
	public static String DATA_FILE = "hdfs://localhost:9000/user/group10/input/input_data.txt";
	public static String OUTPUT_FOLDER = "hdfs://localhost:9000/user/group10/output";
	public static String IMAGE_NAME_PREV = "[Michel]";
}