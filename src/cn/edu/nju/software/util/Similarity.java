package cn.edu.nju.software.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Similarity {
	public static int getImgPosition(List<StringUtil> indexFile, String name, Map<String, Integer> feature) {
		List<String> result = getMatcherImg(indexFile, feature);
		return result.indexOf(name);
	}

	public static List<String> getMatcherImg(List<StringUtil> indexFile, Map<String, Integer> feature) {
		Map<String, Integer> result = new HashMap<String, Integer>();

		for (Map.Entry<String, Integer> entry : feature.entrySet()) {
			String currentCateg = entry.getKey();
			int currentTimes = entry.getValue();

			StringUtil imgs = getImgsByCategary(indexFile, currentCateg);
			for (Map.Entry<String, Integer> imgEntry : imgs.getImgs()
					.entrySet()) {
				String imgName = imgEntry.getKey();
				int imgTimes = imgEntry.getValue();

				int times = imgTimes < currentTimes ? imgTimes : currentTimes;
				if (result.containsKey(imgName)) {
					times += result.get(imgName);
				}
				result.put(imgName, times);
			}
		}

		return sortMap(result, 2);
	}

	public static List<String> sortMap(Map<String, Integer> mapData, int yuzhi) {
		List<Map.Entry<String, Integer>> list_Data = new ArrayList<Map.Entry<String, Integer>>();

		for (Map.Entry<String, Integer> imgEntry : mapData.entrySet()) {
			if (imgEntry.getValue() >= yuzhi) {
				list_Data.add(imgEntry);
			}
		}

		Collections.sort(list_Data,
				new Comparator<Map.Entry<String, Integer>>() {

					@Override
					public int compare(Entry<String, Integer> o1,
							Entry<String, Integer> o2) {
						if (o1.getValue() > o2.getValue()) {
							return -1;
						} else if (o1.getValue() < o2.getValue()) {
							return 1;
						} else {
							return 0;
						}
					}
				});

		List<String> result = new ArrayList<String>();
		for (Map.Entry<String, Integer> entry : list_Data) {
			result.add(entry.getKey());
			System.out.println("name " + entry.getKey() + "  times"
					+ entry.getValue());
		}

		return result;
	}


	public static StringUtil getImgsByCategary(List<StringUtil> indexFile, String categ) {
		for (StringUtil indexLine : indexFile) {
			if (indexLine.getLineNumber().equals(categ)) {
				return indexLine;
			}
		}

		return null;
	}
}
