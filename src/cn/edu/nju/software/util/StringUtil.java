package cn.edu.nju.software.util;


import java.util.HashMap;
import java.util.Map;


public class StringUtil {
	private String number;
	private Map<String, Integer> imgs = new HashMap<String, Integer>();
	
	public StringUtil(String number){
		this.number = number;
	}
	
	public String getLineNumber(){
		return this.number;
	}
	
	public Map<String, Integer> getImgs(){
		return this.imgs;
	}
	
	public StringUtil(String number, String imgsStr){
		this.number = number;
		
		String[] splitStrings = imgsStr.split(";");
		String imgContent;
		for(int i=0; i<splitStrings.length; i++){
			imgContent = splitStrings[i];
			if(imgContent != null){
				String[] keyValue = imgContent.split(":");
				String imgName = keyValue[0];
				int times = Integer.parseInt(keyValue[1]);
				
				addImg(imgName, times);
			}
			
		}
	}
	
	public void addImg(String name, int times){
		imgs.put(name, times);
	}
}

