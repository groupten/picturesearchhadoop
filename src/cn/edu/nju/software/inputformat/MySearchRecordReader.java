package cn.edu.nju.software.inputformat;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.util.LineReader;

import cn.edu.nju.software.ds.ImageInfo;

public class MySearchRecordReader extends RecordReader<Text, ImageInfo>{
	public LineReader in;
	public Text key;
	public  ImageInfo value;
	public Text valueLine;
	public Text keyLine;
	
	@Override
	public void close() throws IOException {}
	
	@Override
	public Text getCurrentKey() throws IOException, InterruptedException {
		return key;
	}
	
	@Override
	public ImageInfo getCurrentValue() throws IOException, InterruptedException {
		return value;
	}
	
	@Override
	public float getProgress() throws IOException, InterruptedException {
		return 0;
	}
	
	@Override
	public void initialize(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {		
		FileSplit fileSplit = (FileSplit)split;
		Configuration job = context.getConfiguration();
		Path file = fileSplit.getPath();
		FileSystem fs = file.getFileSystem(job);
		FSDataInputStream fileIn = fs.open(file);
		in = new LineReader(fileIn,job);
		
		key = new Text();
		value = new ImageInfo();
		valueLine = new Text();
		keyLine = new Text();
	}
	
	@Override
	public boolean nextKeyValue() throws IOException, InterruptedException {
		int lineSize = in.readLine(keyLine);
		if(lineSize == 0) return false;
		String lineStr = keyLine.toString();
		if(lineStr.indexOf(".jpg")!=-1){
			key = keyLine;
		}
		else return false;
		String[] values = new String[4];
		for(int i=0;i<4;i++){
			lineSize = in.readLine(valueLine);
			if(valueLine.toString().indexOf(".jpg") != -1){
				break;
			}
			values[i] = valueLine.toString();
		}
		value.setVectors(values);
		return true;
	}

}
