package cn.edu.nju.software.inputformat;

import java.io.IOException;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.InputSplit;
import org.apache.hadoop.mapreduce.JobContext;
import org.apache.hadoop.mapreduce.RecordReader;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import cn.edu.nju.software.ds.ImageInfo;

public class MyFileInputFormat extends FileInputFormat<Text, ImageInfo>{

	@Override
	public RecordReader<Text, ImageInfo> createRecordReader(InputSplit split, TaskAttemptContext context) throws IOException, InterruptedException {
		return new MySearchRecordReader();
	}

	@Override
	protected boolean isSplitable(JobContext context, Path filename) {
		return false;
	}
	
}